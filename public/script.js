var form = document.getElementById('file-form');
var fileSelect = document.getElementById('file-select');
var titleElement = document.getElementById('p-title');
var buttonTextElement = document.getElementById('btn-text');
var buttonImageElement = document.getElementById('btn-image');
var successElement = document.getElementById('p-success');
var photosElement = document.getElementById('photos');

var firstTitle = "Stuur je selfie door naar de zaal";
var firstButtonText = "Maak een foto";
var firstButtonImage = "camera.svg";

var titles = [firstTitle, "Je foto is onderweg naar de zaal...", firstTitle];
var buttonTexts = [firstButtonText, "Even geduld...", firstButtonText];
var buttonImages = [firstButtonImage, "loading.png", firstButtonImage];
var successDisplays = ["none", "none", "inline-block"];

function showPage(index) {
	titleElement.innerHTML = titles[index];
	buttonTextElement.innerHTML = buttonTexts[index];
	buttonImageElement.src = "images/" + buttonImages[index];
	successElement.style.display = successDisplays[index];
}

form.onsubmit = function(event) {
	showPage(1);
    event.preventDefault();
    var file = fileSelect.files[0];
    var formData = new FormData();
    formData.append('photo', file, file.name);

    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://photos.jonasdeprins.be/upload', true);

    xhr.onload = function () {
      if (xhr.status === 200) {
        showPage(2);
      } else {
        showPage(0)
        alert("Oops! Er ging iets mis, kan je het opnieuw proberen?");
      }
      fileSelect.value = '';
    };

    xhr.send(formData);
}

fileSelect.onchange = function(event) {
    form.onsubmit(event);
}

var socket = io('http://photos.jonasdeprins.be');
socket.on('photo added', function(file) {
    var imageElement = document.createElement('img');
    imageElement.src = 'http://photos.jonasdeprins.be/photo/' + file.filename;

    photosElement.insertBefore(imageElement, photosElement.firstChild);
});

showPage(0);