var current = 0;
var timePerPhoto = 10000;
var didReset = false;
var timeOut;
var isSlideShow = false;

$.getJSON("http://photos.jonasdeprins.be/photo", function( data ) {
	$(data).each(function (key, photo) {
		addPhoto(photo);
	});
});

$(window).keypress(function (e) {
	if (e.key === ' ') {
		isSlideShow = !isSlideShow;
		if (isSlideShow) {
			timeOut = setInterval(nextPhoto, timePerPhoto);
			nextPhoto();
		} else {
			didReset = true;
			current = 0;
			clearInterval(timeOut);
			$('#photos').animate({
				scrollLeft: 0
			});
		}
	}
});

function addPhoto(photo) {
	var url = 'http://photos.jonasdeprins.be/photo/' + photo
	var img = new Image();
	img.onload = function() {
		var left = didReset ? $(window).width() : (current + 1) * $(window).width();
		$('#photos').prepend('<div class="photo" style="background-image: url(' + url + ')" />');
		$('#photos').scrollLeft(left)
		$('#photos').animate({
			scrollLeft: 0
		});
		if (isSlideShow) {
			clearInterval(timeOut);
			timeOut = setInterval(nextPhoto, timePerPhoto);
		}
		didReset = true;
	}
	img.src = url;
	if (img.complete) img.onload();
}

function nextPhoto() {
	current++;
	if (current == $('#photos .photo').length) {
		current = 0
	}
	left = $(window).width() * current;
	$('#photos').animate({
		scrollLeft: left
	});
	didReset = false;
}

var socket = io('http://photos.jonasdeprins.be');
socket.on('photo added', function(file) {
    console.log(file.filename);
    addPhoto(file.filename);
});